/**
 * Sample React Native App with Typescript
 * https://gitlab.com/rn-workshops/step04
 *
 * @format
 */

import React, { Component } from "react";

import { Image, StyleSheet, Text, View } from "react-native";

interface PictureCellProps {
  avatarUri: string;
  userName: string;
  location: string;
  imageUri: string;
  text: string;
}
class PictureCell extends Component<PictureCellProps> {
  render() {
    const { avatarUri, userName, location, imageUri, text } = this.props;
    return (
      <View>
        <View style={{ flexDirection: "row" }}>
          <Image source={{ uri: avatarUri }} style={{ width: 32, height: 32, borderRadius: 16, margin: 8 }} />
          <View style={{ padding: 8, paddingLeft: 4 }}>
            <Text style={{ fontWeight: "600" }}>{userName}</Text>
            <Text style={{ color: "gray" }}>@ {location}</Text>
          </View>
        </View>
        <Image source={{ uri: imageUri }} style={{ height: 250 }} />
        <Text style={{ color: "black", fontSize: 14, margin: 8 }}>
          <Text style={{ fontWeight: "600" }}>{userName}</Text> {text}
        </Text>
      </View>
    );
  }
}

export default class App extends Component<{}> {
  render() {
    return (
      <View style={styles.container}>
        <PictureCell
          avatarUri={"https://gabo.re/rn/avatar1.jpg"}
          userName={"Gabor Wnuk"}
          location={"minsk"}
          imageUri={"https://gabo.re/rn/minsk.jpg"}
          text={"Hello there! This week I'm at Minsk, Belarus!"}
        />
        <PictureCell
          avatarUri={"https://gabo.re/rn/avatar1.jpg"}
          userName={"Gabor Wnuk"}
          location={"warsaw"}
          imageUri={"https://gabo.re/rn/warsaw.jpg"}
          text={"Warsaw, quite a nice place to be!"}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 26,
    flex: 1,
    backgroundColor: "#F5FCFF",
  },
});
